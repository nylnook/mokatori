#!/bin/sh

# Metadata
creator='Nylnook'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=fr
title="Mokatori - Ep. 1 - Business as usual"
series="Mokatori"
seriesindex="1"
tags="comics, graphic novel, climate, climate change, creative commons, libre, free"
description="And if the inhabitants of tomorrow sent us letters?
From 2054 to 2084, from Iceland to Afghanistan, from cataclysms to utopias, five stories draw our possible futures during the \"Mokatori\", the word for climate change among Amazonian Indians
Ep. 1 - \"Business as usual\" - Reykjavik, Iceland, March 20, 2054"
ebookIsbn="979-10-95663-06-5"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
