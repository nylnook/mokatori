#!/bin/sh

# Metadata
creator='Nylnook (aka Camille Bissuel)'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=fr
title="Mokatori - Ép. 0 - La fin"
ebookIsbn="979-10-95663-03-4"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
