#!/bin/sh

# Metadata
creator='Nylnook'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=es
title="Mokatori - Ep. 0 - El fin"
series="Mokatori"
seriesindex="0"
tags="comic, clima, cambio climático, creative commons, libre, gratis"
description="Y si la gente del futuro nos envían cartas ?
De 2054 a 2084, de Islandia a Afganistán, de los cataclismos a las utopías, cinco historias dibujan nuestros futuros posibles para el « Mokatori », palabra para el cambio climático de los indios del Amazonas
Ep. 0 - « El fin » - Mexico, Mexico, 27 de noviembre 2015"
ebookIsbn="979-10-95663-08-9"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
