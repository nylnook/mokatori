#!/bin/sh

# Metadata
creator='Nylnook'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=fr
title="Mokatori - Ép. 1 - Business as usual"
series="Mokatori"
seriesindex="1"
tags="bd, bande dessinée, climat, changement climatique, creative commons, libre"
description="Et si les habitants de l'avenir nous envoyaient des lettres ?
De 2054 à 2084, de l'Islande à l'Afghanistan, des cataclysmes aux utopies, cinq récits dessinent nos futurs possibles pendant le « Mokatori », le mot pour changement climatique chez les indiens d’Amazonie
Ép. 1 - « Business as usual » - Reykjavík, Islande, 20 mars 2054"
ebookIsbn="979-10-95663-05-8"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
