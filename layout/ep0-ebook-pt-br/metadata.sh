#!/bin/sh

# Metadata
creator='Nylnook'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=pt-br
title="Mokatori - Ep. 0 - O fim"
series="Mokatori"
seriesindex="0"
tags="comic, clima, cámbio climático, creative commons, libre, gratuito"
description="E se os habitantes do futuro nos enviam cartas ?
De 2054 a 2084, da Islândia ao Afeganistão, dos cataclismos aos utopías, cinco histórias desenham nossos futuros possíveis durante o « Mokatori », a palavra para o cámbio clímatico para os índios da Amazonía
Ep. 0 - « O fim » - Mexico, Mexico, 27 de novembro 2015"
ebookIsbn="979-10-95663-09-6"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
