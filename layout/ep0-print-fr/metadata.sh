#!/bin/sh

# Metadata
creator='Nylnook'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=fr
title="Mokatori - Ép. 0 - La fin"
series="Mokatori"
seriesindex="0"
tags="bd, bande dessinée, climat, changement climatique, creative commons, libre, gratuit"
description="Et si les habitants de l'avenir nous envoyait des lettres ?
De 2054 à 2084, de l'Islande à l'Afghanistan, des cataclysmes aux utopies, cinq récits dessinent nos futurs possibles pendant le « Mokatori », le mot pour changement climatique chez les indiens d’Amazonie
Ép. 0 - « La fin » - Mexico, Mexique, 27 novembre 2015"
ebookIsbn="000-00-00000-00-0"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
