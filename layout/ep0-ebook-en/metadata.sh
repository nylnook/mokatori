#!/bin/sh

# Metadata
creator='Nylnook'
url="www.nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=en
title="Mokatori - Ep. 0 - The End"
series="Mokatori"
seriesindex="0"
tags="comics, graphic novel, climate, climate change, creative commons, libre, free"
description="And if the inhabitants of tomorrow sent us letters?
From 2054 to 2084, from Iceland to Afghanistan, from cataclysms to utopias, five stories draw our possible futures during the \"Mokatori\", the word for climate change among Amazonian Indians
Ep. 0 - \"The End\" - Mexico City, Mexico, November 27, 2015"
ebookIsbn="979-10-95663-04-1"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
dirartwork=artwork
