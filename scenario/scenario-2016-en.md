# Mokatori
### Our possible futures

[Legal Information]
[Dedication] You can't predict the future, but you can achieve it
[Thanks]

## Opening – an illustration
The Earth, and inside it two fetuses : one of a baby dinosaur, one of a baby human. 

[Subtitle] "Last time there was as much carbon dioxide (CO2) in the atmosphere as there is today... was 3 million years ago*. Dinosaurs were not even born." [source : Gilles Ramstein, Voyage à travers les climats de la terre - Travel through the earth climates, p.282]


## Ep. 0 - "The End" [Introduction, a mexican worker on a building site]
*Mexico City, Mexico, November 27, 2015*

When I was 17, at the end of the twentieth century, my mother told me that I was part of the generation that could no longer believe in anything. Good or bad, the greatest ideologies of the twentieth century had collapsed or were in the process of collapsing: Nazism, communism, anarchisms, the religions of the Book… Walls had fallen, but capitalism remained, with an illusion of democracy. Not that it's good, but no one came up with something better. The time for ideas was over, and politics were meaningless.

Since I was born, we never had a period longer than 3 years without an economic crisis, in my country or anywhere else in the world.

I didn't know at that time I would experience at least two major revolutions in my life: the digital revolution and the climatic revolution… That I would have to fight just to be able to breathe without poisoning myself. And also to preserve the freedom I was given.

I'm not sure at all, but I feel like it's the end of a world. It's time to change logic. And a lucky find will force me to make that change… 
At the city dump I found a smartphone that still works, and from time to time, I receive letters from the future…


## Interlude – an illustration
Fukushima, March 11, 2057
Ceremony for celebrating the end of the nuclear power plant decommissioning


## Ep. 1 - "Business as Usual" [Probable letter, from an old man emigrated to Iceland]
*Reykjavik, Iceland, March 20, 2054*

If these few positrons cross time and correctly transform themselves into electrons 40 years earlier, you, the one who will find this digital message, don't need to worry too much about your own future, the main part of humanity is still healthy today. I'm writing to you because I would have loved to have received a letter like this one, when I was young, when I was so anxious to find a job, asking myself what was going to happen to me, and if the planet would survive the parasites we are.
Well, the answer is yes, even if there were some small changes. We are now 10 billion humans. The IPCC* with its politically correct forecasts and its caution turned out to be wrong of course: what its scientists had predicted for year 2100 is already today's reality. There were many “crises”, but we eventually ended up abandoning that word, which didn’t really make sense for a permanent state. 
Today we say we are "in remission" and we are not doing such a bad job doing this. Presumably, it's set to go on for at least another 300 years*, but I won't be around anymore to tell you. My grandchildren will suffer even more of the heat, because since we have given the impulse to the planet, it's warming up all by itself.

Concerning me, I emigrated to Iceland about ten years ago, and during this spring day, we have a pleasant 12°C (53.6 °F), the almond trees are in blossom. After the permanent opening of the two Arctic sea routes, the population increased considerably here.
Compared to your time, it's currently the new French Riviera… The only disadvantage is that there is very little sunshine during winter, approximately 5 hours a day at the winter solstice. But we catch up in the summer, when it never gets dark and festivals never end.
Well, of course, one has to be able to pay for living here. It takes approximatively forty year with a good salary to pay for an apartment. But since we are almost all old and rich, it's fine.
We do not suffer from smog neither, which still covers all the former mega-cities, because the town was sparsely populated during the peak oil* years, and the ocean also brings some fresh air. Of course we don't use oil anymore [picture of a barrel priced over $ 3,000], except for international super-freighters. Bikes, often electric, are popular, my grandchildren never passed their driving license. I doubt they will ever need one: trucks are all self-driven. For long trips we use velomobiles, to transport children cargo bikes, and when we get too old, we sit in the back of cycle rickshaws.
Fortunately we don't have the Paris, London and Brussels radioactivity. By the way, if possible avoid the Paluel* area, especially in the year 2028.
While I'm at it, to build your future, you should also forget the Pacific islands, Venice, Alexandria, Port Said, New Orleans, Bangkok, Astrakhan, Dhaka, Ho Chi Minh City and the whole south of Vietnam, Foshan, Calcutta, and all the cities that are for you yet more or less 500 km (300 miles) far from the Sahel if you want to be able to live in the daytime… I hope I haven't forgotten any.
I'm still working half-time, and I finished my career working for bottled water companies. Our main source is located in nearby Greenland. But don't worry, we are now environmentally friendly, and use good old glass bottles, plastic isn't fantastic anymore. In Africa and Asia the demand is thriving.
What I miss the most from your time are the critters. I mean, real ones, not the GMO lynx-cats that everyone has at home today. They are beautiful thought, but it's not the same. I am not speaking of invasive species like the mosquitoes that ruin our lives. No, I mean the wild ones.
I didn't pay much attention to them when I was young. For example, I would never have imagined that I would miss, among others, starfish. And honey too… I ate so much when I was young.
Well, anyway, it's all right, we aren't the worst off. If you want my advice, you should take care on two things: first, you'll have to defend freedom, your political and your digital freedoms. Personally before I left France I couldn't stand the "report" buttons on Facebook anymore. This is fighting against others.
The second thing is that you will have to learn sobriety, whether you like it or not. This will be a fight against yourself.
Personally, since I have read Epicurus, I could manage much better with this. He says that the key to happiness is to act soberly and to know your own limits.
In short, that's life. Typhoons don't always fall onto people.
Now I must stop, I won't have enough positrons to send the message otherwise. I hope we'll get the opportunity to meet. Please give me your opinion about this message. May be this will happen within the next 40 years for you, may be just within a moment for me. I wish you fair winds, nice warm fair winds! 


## Interlude – an illustration
Svalbard Global Seed Vault/ Navdanya Farm
"Svalbard Global Seed Vault, opened since 2008, store at -18 °C (−0.4 °F) about 2,000 global seed varieties for hundreds of years.*"
"Navdanya Agrobiodiversity Conservation and Ecological Farm in India, safeguards, produce and share approximately 1500 varieties of regional seeds, and helped hundreds of similar projects to be born worldwide since the beginning of its activities in 1995.*"


## Ep. 2 - "Time Machine" [Dystopian Letter, a Taiwanese Physicist (she)]
*Taipei, 臺北市, Taiwan, November 13, 2084*

Dear inhabitant of the past, dear inhabitant of the possible,

My name is Lei Lung-pin and I'm a chrono-physicist. I am writing to you in the mad hope of reaching the right person, one who believes in my message and will act to prevent the worst from happening.
I know how to travel in time. I figured out how to build a machine capable of moving people and objects in that dimension. 
But I'm the only one who knows.

For the last six years I've been working for my country's Government, the Republic of China*, also known as the Island of Taiwan.
6 years ago, the delegation of the South-East Asian countries and the Pacific Islands secretly met in Taipei under the leadership of our President, and declared war on the People's Republic of China in the year 1992. Yes, in its madness, my nation with others declared the first trans-temporal war! The program remained secret, the current other China* didn't discover anything and knows nothing of what's happening here.

As a star pupil of Tsai Zhao, the first chrono-physicist who discovered the “simple" message in time 36 years ago, 
I was hired and my researches were funded to make this war possible.

This technology was so promising that our president saw the perfect solution to all our environmental problems.
Indeed, he is convinced that rising water levels were caused in large part by the other China's economic explosion earlier this century.
The rising water has flooded our coast and our beautiful city of Taipei many times, and wiped several Pacific islands off the map. Flooding has also caused all our problems with the drinking water supply over recent decades. It's true that the other China suffocated us with its pollutionand and brought us thirst with its embargo on water, to the point that we no longer counted the deaths. Those who only had sea water to drink became dehydrated, and the alternative was to drink acidic, toxic and polluted rainwater…
Unfortunately technology has kept its promises, and I have been its most zealous disciple.
Of course the official speech omits to say that our China also participated in this pollution at the same period. Their plan is to simply destroy major cities of the continent with atomic weapons in 1992, in order to nip in the bud any economic development and to drastically reduce the population. So they intend to rewrite history prevent the worst of the climate crisis. Supposedly, 90 years of atomic radiation 300 km (186 miles) from our shores would not be a problem! But even regardless of radiation, in addition to the heinous and genocidal crime this represents, changing our history has unimaginable consequences.

I accepted the job because I wanted to improve my knowledge, master my discipline, and understand if the mystery of time could really be unraveled. Of course I didn't mind having a comfortable life and water filtered by reverse osmosis, which only the elite have access to. But it's also because I wanted to avenge the death of my loved ones. Somehow, I didn't believe I could do this. But five months ago I found it. I haven't told anyone, not even my colleagues. I was the only one in the lab that night, when a small accident gave me the solution.

The lab, our facilities, I set it all on fire. The time travel machine will not be built here and now, because tonight I will jump from Bitan Suspension Bridge. I refuse to give birth to this technology, to make possible this unspeakable “solution”. A technology so complex, so full of itself, so powerful, so distant from any intuition and from any life must not be allowed to exist. I believe it's our blind faith in our power, our science and our technology that led us to this impasse.
If there are solutions, they are simple and accessible to all, like settling for what we have, for the joy of being here and now, like accepting that every atom is alive, unique and precious (whether it forms stone or flesh), like understanding that we are all interconnected parts of “nature” itself.
In your time, some modesty and decency was enough. Do not burn all this energy for so little. Today we have to use an ultimate weapon to try to escape our fate.
I will put an end to my life tonight, but I wish I had never been born. I wish that all this was only a bad dream, and would remain a bad dream for the rest of time. I wish that your actions create another possible world.
A world where nobody will be thirsty.


## Interlude – an illustration
a survivor : the battle over the last barrels of oil in the Persian Gulf


## Ep. 3 - "In the Shade of Fig Trees" [Utopian Letter, a young Greek Mom]
*Farsala, Φάρσαλα, Greece, July 17, 2077*

Hi ancestor!
My grandmother just told me that in your time, my country was under the dictatorship of capital after the crisis of the Great Recession of 2008/2009… I told myself that a small message of comfort, even though I do not know who is going to receive this "bottle from future", would certainly do you good.
I just had a baby girl, now 3 months old, and I intend to enjoy the 2 years coming to finish reforesting the garden. This is my second daughter, and it will be my last one, because our European birth rate policy strongly penalizes having more than 2 children… Our family is not eligible for an exemption, Earth is already overpopulated, and then two girls occupy and satisfy me quite enough already!
Today it's 49 °C (120 °F) outside, partially because of you. I do not thank you, but we found solutions: we reforest as much as we can, which still makes us reduce a few degrees with some shade, and it absorbs CO2 too. In well-designed and well insulated houses it's fresh, and otherwise the solar air conditioners* are now widespread. So I'm not afraid for my little one, we remain sheltered inside in the summer.
[visual break]
As I told you, I don't worry about the future, because more than two generations ago, us Greeks adopted universal basic income*. Everyone receives a small salary every month, unconditionally, regardless of age or if one is working or not. We no longer know misery, we go to work for fun and to supplement this income, and no one is really exploited anymore. Your society, based on the threat of unemployment, and the threat of being made homeless, no longer exists. Pensions and scholarships also disappeared, no longer needed. My daughters also receive a basic income, half of what over 16's receive. Apart from childhood, you now work all your life, only when you can and when you want. But no one can be paid more than 1000 hours per year, to share the work, avoid waste, and allow natural areas to regenerate. And the country is much richer that way. Besides there is no such thing as a rich man any more: the basic income is accompanied by a maximum income limit of 10 times the basic amount.
We were the first ones in Europe along with the Finns, then some countries adopted it. Well Namibia and the Kingdom of Bhutan had done it before us, but we still led the way, probably because of our history, and our fight for the “debt” refusal.
I'm not saying that everything is rosy, life here is no picnic and the way we live would probably seem strange to you: we do not use any fossil fuels or nuclear power anymore (until I was 22 years old I had never seen oil!), sail and pedal cars are rented in sharing services, and the rest of the services (water, purification, electricity, phone and Internet) is mutualized and managed by the city. It's forbidden to privatize common goods since… I don't know when. Maybe since the abolition of stock and exchange markets in 2062.
Fo example, for the city's new digester* (biogas plant), all inhabitants have contributed to fund it, in exchange for the future right to sell their own organic waste. So everyone wins.
And we are united and repair a lot. By the way I worked at the recycling center until my 7th months of pregnancy. I think it was a waste reception center at the beginning of the century, then a FabLab* was attached to it, and now that landfills don't exist anymore it has become a large repair, processing, recycling and production center. There is quite a lot of work over there, and it's always a pleasure to create something new with something old. That is something robots can't do! This is also where climate refugees come to work with us and learn our language, we always need hands.
But my little one is waking up and my husband just returned with our basket from the CSA (Community-supported agriculture), I'll leave you now… Look, there are figs again! I wish you a future as wonderful as my present!


## Interlude – an illustration
an Indian farmer commits suicide by drinking Roundup*


## Ep. 4 - "Masters of the Stratosphere" [Letter in which nothing has changed, an African engineer]
*Walvis Bay, Namibia, February 29, 2060*

Dear old Pa,
as the saying goes "most people prefer to live with old problems than with new solutions", but here we are doing very well: 
we use new solutions to maintain old problems. That way you don't ask yourself too many questions. Geoengineering* it's called. Now humans control the climate.

With the good education you made me finish, it has now been 10 years that I have worked here in the new international spaceport of Walvis Bay. 
My job is to send rockets full of sulfur dioxide (SO2) in the stratosphere: it cools it a little bit. When we can we also send mirror satellites in space. Another team is responsible for causing the creation of marine clouds [with hoses of a kilometer long] that reflect light. We always fight for our rockets' launch windows, we don't want any of their clouds to get in the way!
There is yet another team that scatters reflective micro-bubbles in the ocean. Well, the greenies moaned in the name of the last fish, but there were other priorities. There are only two astro-climatic bases like this in the world, us and our colleagues in Longueuil, Quebec. One in the northern hemisphere, one in the southern hemisphere. Both are placed on ocean currents.
All this just serves to send back sun rays, so as not to increase the temperature of our small planet even more.
That costs several mountains, but seeing that all the multinationals of the world buy our carbons credits*, and seeing what they emit, we are in no risk of running out of cash.
I have job security, for sure… If I ask for it, my children, my grandchildren and my great grandchildren will even get a job here. Because we should certainly not stop, otherwise the heat will rise exponentially*.
My dear old Pa, I'm only writing this down because today I have doubts about the way you showed me. This morning, the doctor told me I had cancer. I probably handled too much SO2. 10 years, it's gone so fast. Maybe I should have been a farmer, like you, or even a permaculturist*. I would have spent my life in the fields. If you get this message, will you tell the young me?


## Interlude – an illustration
[Surreptitious advertising :] Let's build a better energy future. Let's go. 
[Photo of an aerial view of shale gas fracking sites]
[Photo : Bruce Gordon / EcoFlight / CC-BY-SA / https://www.flickr.com/photos/sfupamr/14601885300]


## Ep. 5 - "Living" [Letter in which we changed civilization, a young Afghan adult (she - 22 years)]
*Kunduz, کندز, Afghanistan, April 3, 2072* [in a forest-town]

Hello old man! I forgive you.
I forgive you the heat, I forgive you the thirst, I forgive you the hunger, I forgive you the desert which wins and the sudden torrents of rain. Because together we have planted and seen the seeds of our future and our peace grow.

You came to fight in my home land and we fought against you, but I forgive you in the name of my parents and grandparents.
You did not know ! You believed you were separated from the world, you thought that matter was inert, and thus you treated your world with contempt as something lifeless.
That was before, before the 4th injury*:
- The cosmological injury: Copernicus, the Earth is not the center of the universe
- The biological injury: Darwin, the Man is the fruit of evolution and he is an animal like any other
- The psychological injury: Freud, the Man is not master of his own thoughts, he is dominated by his unconscious
- The neuro-quantum injury: Zhao, the Man is not more intelligent than what surrounds him, all matter is alive and conscious, both in us and outside us, only the intensity of its interaction with spacetime gives us the impression of inert or alive on our scale (mineral, plant, animal…).

Difficultly, we learned that we are fundamentally the same life as the land on which we stand, whether we call it Allah or Gaïa, that we are interdependent, that my consciousness does not exist without your consciousness and that of the tree next to me and those at the other end of our planet. From then on, killing the foreigner or paying to pollute the distant tree, is like destroying or hurting ourselves: it makes no sense and it is suicidal!
And from then on we began to forgive, to heal from the war and the horrors of past.

"The sea transport bubble burst", followed by "the big stop" : this is the beautiful name we gave to the shock of the collapse of the globalized western economy. I was 9 years old. In 2059, after decades of super-containers which moved the goods of the world at a near zero price, the depletion of fossil fuels could no longer be offset by subsidies and financial tricks, and transportation abruptly became expensive, very expensive. Your entire civilization stopped within a few weeks.
But for us, it did not make much difference: we were already in poverty, we already knew what to do and we depended only on ourselves. Then, in our way, we helped you to understand what a Taiwanese chrono-physicist had discovered by what we have always known, and then, all together we planted the seeds of our future. Suddenly, our awareness of ourselves changed. Suddenly I was able to study, me a girl!

I see you do not understand, let me try to explain.
In your time, even if it had almost been a century since Einstein and his colleagues began to theorise about it, you still hadn't understood quantum physics. You thought that photons and electrons behaved randomly, could be at the quantum level both present and absent, here and there… That Schrödinger's cat was *both* alive and dead at the same time, or that he was in parallel worlds ! All this because their strange behavior was not determined by another force, another cause, and they escaped the logic of the “normal” physical world.
In the absence of logic, your only explanation was that particles behave in a random way, but with statistical probability which would allow you to predict more or less what should occur. Even if to be both there and not there at the same time, that seems absurd, statistic saved the scientific !
But that's because you thought that photons and electrons were lifeless, unconscious, inert, purely "mechanical". The solution is quite simple, and our current physics explain it: it's the photon which chooses its electron. Even though it seems  inconceivable to you, it is the one who decides. It is intelligent. And we share its intelligence in ourselves, in the electrons that run through our neurons and are exchanged. There are electrons in all atoms, and there are atoms in any matter. Therefore all matter is intelligent and alive, even if it crosses spacetime slowly for us, for example if it is in mineral form.

We perceive this intelligence and this life around us while meditating. It gives us the strength to repair together our collective wounds, and those of our land. We are no longer separate. Don't let yourself be called consumer anymore, one who consumes and devours the life around him and in him: let's call you gardener, one who makes life grow around him and in him. Then you will find that you are part of the life all around you, and you will not feel lonely or separated anymore from your Earth, not from people close to you, and not even from people far away from you…
Goodbye, and peace be upon you!

## Final – an illustration
a pregnant woman making love - below her, the Mexican worker

[Notes]


