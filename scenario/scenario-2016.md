# Mokatori
### Nos futurs possibles

[Infos légales]
[Dédicace] Vous ne pouvez pas prévoir l'avenir, mais vous pouvez le réaliser
[Remerciements]

## Ouverture – une illustration
La Terre, et en elle deux fœtus : un bébé dinosaure et un bébé humain.

[Sous-titre] "La dernière fois qu'il y a eu autant de dioxyde de carbone (CO2) dans l'atmosphère… c'était il y a 3 millions d'années*. Les dinosaures n'étaient pas encore nés." [source : Gilles Ramstein, Voyage à travers les climats de la terre, p.282]


## Ép. 0 - "La fin" [Introduction, un ouvrier mexicain sur un chantier]
*Mexico, Mexique, 27 novembre 2015*

Quand j’avais 17 ans, à la fin du XXe siècle, ma mère m’a dit que je faisais partie de la génération qui ne pouvait plus croire en rien. Bonnes ou mauvaises, les grandes idéologies du XXe siècle s’étaient effondrées ou étaient en train de le faire : le nazisme, le communisme, les anarchismes, les religions du livre… Les murs étaient tombés, mais il ne restait que le capitalisme, par défaut. Ce n’est pas que c’est bien, mais on a rien trouvé de mieux, avec un vernis de démocratie. Le temps des idées était fini, et la politique était insignifiante.

Depuis que je suis né il ne s’est pas passé plus de 3 ans sans qu’il y ait une crise économique, dans mon pays ou quelque part dans le monde.

Je ne savais pas alors que j’allais vivre au moins deux grandes révolutions dans ma vie : la révolution numérique et la révolution climatique… Que je devrais me battre pour pouvoir simplement respirer sans m’empoisonner. Et aussi pour conserver la liberté qu’on m’avait donnée.

Je n’en suis pas certain, mais je sens la fin d’un monde. Il est temps de changer de logique. Et une trouvaille va me forcer à le faire… À la déchetterie, j’ai récupéré un smartphone qui marche encore [il y a un chat de Schrödinger au dos], et de temps en temps, je reçois des lettres du futur…


## Intermède – une illustration
Fukushima, le 11 mars 2057
Cérémonie qui marque la fin du démantèlement de la centrale nucléaire


## Ép. 1 - "Business as usual" [Lettre du probable, un vieil homme émigré en Islande]
*Reykjavík, Islande, 20 mars 2054*

Si ces quelques positrons traversent le temps et se transforment bien en électrons 40 ans plus tôt, toi qui trouveras ce message numérique, ne t’inquiète pas trop pour ton avenir, l’essentiel de l’humanité est encore en bonne santé aujourd’hui.
Je t’écris car plus jeune j’aurais bien aimé recevoir une lettre comme celle-ci, tout angoissé que j’étais de me trouver un travail, me demandant ce qui allait me tomber sur le coin de la figure, et si la planète survivrait aux parasites que nous sommes.
Eh bien, la réponse est oui, même s’il y a eu de petits changements. Nous sommes maintenant 10 milliards d'humains. Le GIEC* avec ses prévisions politiquement correctes et sa prudence s’est bien sûr trompé, et ce que ses scientifiques annonçaient pour 2100 est déjà la réalité aujourd’hui. Il y a eu beaucoup de “crises”, mais on a fini par abandonner ce mot, inadapté à un état permanent. On dit aujourd’hui qu’on est « en rémission » et on ne s’en sort pas si mal. *A priori* c’est parti pour durer au moins encore 300 ans*, mais je ne serai plus là pour te le raconter. Mes arrières petits-enfants vont avoir encore plus chaud, car maintenant qu’on lui a donné de l’élan, la planète se débrouille toute seule pour se réchauffer.
Personnellement, j’ai émigré depuis une dizaine d’année en Islande, et en ce jour de printemps, il fait un agréable 12 °C et les amandiers sont en fleur. Depuis l’ouverture permanente des deux routes maritimes de l’Arctique, la population a beaucoup augmenté ici.  Par rapport à ton époque, c’est la nouvelle Côte d’Azur… Le seul inconvénient est le peu de temps d’ensoleillement l’hiver, environ 5h au solstice d'hiver. Mais on se rattrape l’été, il n’y a pas de nuit et les festivals ne s’arrêtent jamais.
Bon, bien sûr, il faut payer sa place. Compter une quarantaine d’année d’emprunts avec un bon salaire pour se payer un appartement. Mais comme on est presque tous vieux et riches, ça va.
On ne souffre pas non plus du brouillard de pollution qui recouvre encore toutes les anciennes mégapoles car la ville était peu peuplée pendant les années du pic pétrolier*, et l’océan amène de l’air frais. Bien sûr, on n'utilise plus de pétrole [image prix du baril supérieur à 3000 $], sauf pour les supers-cargos de transport internationaux. Le vélo, souvent électrique, est très populaire, mes petits-enfants n’ont jamais passé leur permis de conduire. Ça m’étonnerait qu’ils le passent un jour : les camions qui restent conduisent tout seuls. Pour les grands trajets on utilise des vélomobiles, pour transporter les enfants des vélos cargos, et quand on a passé l'âge, on s'assoit à l'arrière des vélotaxis.
On n’a pas non plus la radioactivité de Paris, Londres et Bruxelles, heureusement. Au fait, si possible évite la zone de Paluel*, surtout en 2028.
Pendant que j’y suis, pour te construire un avenir, oublie aussi les îles du Pacifique, Venise, Alexandrie, Port Saïd, la Nouvelle-Orléans, Bangkok, Astrakhan, Dhaka, Ho Chi Minh Ville et tout le Sud du Vietnam, Foshan, Calcutta, et toutes les villes qui sont encore pour toi à plus ou moins 500 km du Sahel si tu veux pouvoir vivre durant la journée… J’espère que je n’en oublie pas.
Je travaille encore à temps partiel, et j’ai fait toute la fin de ma carrière dans l’eau en bouteille. Notre principale source est au Groenland voisin. Mais rassure-toi, c’est écologique, on fait dans la bonne vieille bouteille en verre maintenant, le plastique ça n’est plus fantastique. Et puis la demande en Afrique et en Asie est florissante.
Ce qui me manque le plus de ton époque, ce sont les bestioles. Je veux dire les vraies, pas les chats-lynx OGM que tout le monde a chez soi aujourd’hui. Ils sont beaux mais ce n'est pas pareil. Pas non plus les espèces envahissantes comme ces moustiques qui nous pourrissent la vie. Non, les sauvages. Pourtant je n’y accordais pas beaucoup d’attention quand j’étais jeune. Par exemple, je n’aurais jamais imaginé que je regretterais les étoiles de mer, parmi tant d’autres. Et puis, le miel aussi… Qu’est-ce que j’ai pu en manger quand j’étais petit.
Bref, ça va bien, on n'est pas les plus mal lotis. Si tu veux un conseil, il faudra juste que tu veilles bien à deux choses : tout d’abord, tu vas devoir défendre ta liberté, politique et numérique. Personnellement je ne supportais plus les boutons “signaler” sur Facebook en France avant de partir. Ça c’est le combat contre les autres.
L’autre chose, c’est que tu vas apprendre la sobriété, que tu le veuilles ou non. Ça c’est le combat contre toi-même. Personnellement, depuis que j’ai lu Épicure, je m’en accommode bien mieux. Il dit que la clef du bonheur est d'agir sobrement et de connaître ses propres limites.
Bref, c'est la vie. Et puis, les typhons ne tombent pas sur tout le monde.
Je dois m’arrêter là, je ne vais pas avoir assez de positrons pour envoyer le message sinon. J’espère qu’on aura l’occasion de se rencontrer, tu me donneras ton avis sur ce message. Peut-être dans 40 ans pour toi, peut-être tout à l’heure pour moi. Je te souhaite bon vent, et un bien chaud !


## Intermède – une illustration
La réserve mondiale des semences au Svalbard / la ferme de Navdanya 
"La chambre forte mondiale de graines du Svalbard, ouverte depuis 2008, conserve à -18°C environ 2000 variétés de graines du monde entier pour des centaines d'années.*"
"La ferme de conservation de la biodiversité de Navdanya en Inde, sauvegarde, produit et partage environ 1500 variétés de graines régionales, et a aidé des centaines de projets similaires à naître de par le monde depuis le début de ses activités en 1995.*"


## Ép. 2 - "Machine à remonter le temps" [Lettre dystopie, une physicienne taïwanaise]
*Taipei, 臺北市, Taïwan, 13 novembre 2084*

Cher habitant du passé, cher habitant des possibles,

Je m’appelle Lei Lung-Pin et je suis chrono-physicienne. Je vous écris dans l’espoir fou de tomber sur la bonne personne, celle qui croira en mon message et pourra agir pour empêcher le pire d’arriver.
Je sais comment voyager dans le temps. J’ai compris comment construire une machine capable de déplacer des hommes et des objets dans cette dimension-là. Mais je suis la seule à savoir.

Depuis 6 ans je travaille pour le gouvernement de mon pays, la République de Chine*, connue aussi sous le nom d'Île de Taïwan.
Il y a 6 ans, la délégation des pays de l’Asie du sud-est et des îles du Pacifique s'est réunie secrètement à Taipei sous l’égide de notre président, et a déclaré la guerre à la République populaire de Chine de l’an 1992. Oui, dans sa folie, ma nation avec d’autres a déclaré la première guerre trans-temporelle ! Le programme est resté secret, l’autre Chine* actuelle n’a rien découvert et n’a aucune idée de ce qui se passe ici.

En tant qu’élève brillante de Tsai Zhao, la première chrono-physicienne qui a découvert le “simple” message dans le temps il y a 36 ans, j’ai été embauchée et depuis mes recherches ont été financées pour rendre cette guerre possible.

Cette technologie était si prometteuse que notre président a vu en elle une parfaite solution à tous nos problèmes environnementaux. En effet, il est convaincu que la montée des eaux a été causée en grande partie par l’explosion économique de l’autre Chine au début du siècle. 
La montée des eaux a inondé nombre de fois nos côtes et notre belle ville de Taipei, et a effacé de la carte plusieurs îles du Pacifique. Les inondations ont aussi engendré tous nos soucis de ravitaillement en eau potable ces dernières décennies. C’est vrai que l’autre Chine nous a asphyxiés avec sa pollution et nous a assoiffés avec son embargo sur l’eau, jusqu'au point de ne plus compter les morts. Ceux qui ne pouvaient plus boire que de l’eau de mer se déshydrataient, et l’autre alternative était de boire de l’eau de pluie très acide, toxique et polluée…
Malheureusement la technologie a tenu ses promesses, et j’en ai été la disciple la plus zélée.
Bien sûr le discours officiel omet de dire que notre Chine a aussi participé à cette pollution à la même époque. Leur projet est tout simplement de détruire les grandes villes du continent à l’arme atomique en 1992, de façon à étouffer dans l’œuf tout développement économique et de réduire drastiquement la population. Ils projettent donc de réécrire l’histoire pour obtenir des conséquences climatiques plus bénéfiques. Soi-disant, 90 ans de radiations atomiques à 300 km de nos côtes ne serait pas un problème ! Mais même sans en tenir compte, en plus du crime atroce et génocidaire que cela représente, changer ainsi notre histoire a des conséquences inimaginables.

J’ai accepté le poste car je voulais améliorer mes connaissances, aller au bout de ma discipline, et comprendre si le mystère du temps pouvait vraiment être percé. Bien sûr, ça ne m'a pas dérangée d’avoir une vie confortable et de l’eau filtrée par osmose inverse, à laquelle seule l’élite a accès. Mais c’était aussi parce que je voulais venger la mort de mes proches.
Quelque part, je ne croyais pas que j’y arriverais. Mais cela fait maintenant 5 mois que j’ai trouvé. Je n’en ai parlé à personne, pas même à mes collègues. J'étais seule au labo cette nuit-là, quand un petit accident m’a donné la solution.

Le labo, nos installations, je viens d’y mettre le feu. La machine à voyager dans le temps ne sera pas construite ici et maintenant, car cette nuit je sauterai du Pont suspendu de Bitan. Je refuse de donner naissance à cette technologie, de rendre possible cette “solution” innommable. Une technologie aussi complexe, aussi imbue d’elle-même, aussi puissante, aussi éloignée de toute intuition et de toute vie, ne doit pas être autorisée à exister. Je crois que c’est notre foi aveugle en notre pouvoir, notre science et notre technologie qui nous a conduits dans cette impasse.
S’il y a des solutions, elles sont simples et à portée de tous, comme nous contenter de ce que nous avons, de la joie d’être ici et maintenant, comme accepter que chaque atome soit vivant, unique et précieux (qu'il forme de la pierre ou de la chair), comme comprendre que nous sommes tous des parties inter-connectées de “la nature” elle-même.
À votre époque, il suffisait d’un peu de modestie et de décence. Ne pas brûler autant d'énergie pour si peu. Aujourd’hui nous devons employer une arme ultime pour essayer d’échapper à notre destin.
Je mets fin à ma vie ce soir, mais je souhaiterais que mon existence n’ait jamais eu lieu. Je souhaite que tout ceci n’ait été qu’un mauvais rêve, et le reste à jamais. Je souhaite que vos actes créent un autre monde possible.
Un monde où personne n’aura soif.


## Intermède – une illustration
un survivant : la lutte autour des derniers barils de pétrole dans le golfe persique


## Ép. 3 - "À l'ombre des figuiers" [Lettre utopie, une jeune maman grecque]
*Farsala, Φάρσαλα, Grèce, 17 juillet 2077*

Salut mon ancêtre !
Ma grand-mère vient de me raconter qu’à votre époque, mon pays subissait la dictature du capital après la crise de la Grande Récession de 2008/2009… Je me disais qu’un petit message de réconfort, même si je ne sais pas qui va recevoir cette « bouteille du futur », vous ferait sûrement du bien.
Je viens d’avoir une petite fille, qui a maintenant 3 mois, et je compte bien profiter des 2 ans qui viennent pour finir de reboiser le jardin. C’est ma seconde fille, et ce sera ma petite dernière, car notre politique nataliste européenne sanctionne fortement le fait d’avoir plus de 2 enfants… Notre famille n’a pas droit à une dérogation, la Terre est déjà trop peuplée, et puis deux filles ça m’occupe et me satisfait déjà bien assez !
Aujourd’hui il fait 49 °C dehors, en partie grâce à toi. Je ne te remercie pas, mais heureusement on a trouvé des solutions : on reboise autant que l'on peut, ce qui nous fait toujours gagner quelques degrés et de l’ombre, et puis ça absorbe du CO2 aussi. Dans les maisons bien conçues et bien isolées on est au frais, et sinon les climatisations solaires* sont répandues maintenant. Donc je ne crains pas pour ma toute petite, on reste à l'abri à l’intérieur pendant l’été.
[pause visuelle]
Comme je te disais, je ne m’inquiète pas pour l’avenir, car depuis plus de deux générations maintenant, nous les grecs on a adopté le revenu de base universel*. Tout le monde touche un petit salaire tous les mois, sans conditions, quel que soit son âge, qu’il travaille ou non. On ne connaît plus la misère, on va au travail pour le plaisir et pour compléter ce revenu, et on ne se laisse plus exploiter. Votre société, basée sur la menace du chômage, c’est-à-dire la menace d’être mis à la rue, n’existe plus. Les retraites et les bourses d’études ont disparu aussi, on n'en a plus besoin. Mes filles touchent également un revenu de base, la moitié de celui des plus de 16 ans. En dehors de l’enfance, on travaille maintenant toute sa vie, seulement quand on le peut et quand on le veut. Mais on ne peut pas être payé plus de 1000 heures par an, pour partager le travail, éviter le gaspillage, et permettre aux milieux naturels de se régénérer. Et le pays est bien plus riche comme ça. D'ailleurs il n'y a plus vraiment de riches : le revenu de base est accompagné d'une limite de revenu maximum de 10 fois le montant de base.
On a été les premiers en Europe avec les Finlandais, puis quelques autres pays l’ont adopté. Bon, la Namibie et le Royaume du Bhoutan l’avaient fait avant nous, mais on a quand même montré le chemin, probablement à cause de notre histoire, et de notre combat pour le refus de la “dette”.
Je ne dis pas que tout est rose, la vie ici n’est pas de tout repos et notre façon de nous organiser te paraîtrait probablement étrange : nous n’utilisons plus aucune énergie fossile ni nucléaire (jusqu’à mes 22 ans je n’avais jamais vu de pétrole !), les voitures à voiles et à pédales sont louées en service de partage, et le reste des infrastructures de service (l’eau, l’assainissement, l’électricité, le téléphone et Internet) sont mutualisés et gérés par la ville. Il est interdit de privatiser des biens communs depuis… je ne sais plus quand. Peut-être depuis l’abolition de la Bourse en 2062.
Par exemple, pour le nouveau digesteur* (usine de méthanisation) de la ville, tous les habitants ont cotisé pour le financer, en échange du droit futur de lui vendre leurs propres déchets organiques. Du coup tout le monde y gagne.
Et puis nous sommes solidaires et nous réparons beaucoup. D’ailleurs je travaillais à la recyclerie jusqu’à mes 7 mois de grossesse. Je crois que c’était une déchetterie au début du siècle, puis un FabLab* y a été accolé, et maintenant que les déchetteries n’existent plus c’est devenu un grand centre de réparation, transformation, recyclage et production. Il y a pas mal de boulot là-bas, et c’est toujours un plaisir de créer du neuf avec du vieux. Ça les robots ne savent pas le faire ! C’est aussi là que les réfugiés climatiques viennent travailler avec nous et apprendre notre langue, on a toujours besoin de mains.
Mais ma petite se réveille et mon époux vient de rentrer avec notre panier de l’AMAP*, je te laisse… Tiens il y a encore des figues ! Je te souhaite un futur aussi merveilleux que mon présent !


## Intermède – une illustration
un paysan indien se suicide au Roundup*


## Ép. 4 - "Maîtres de la stratosphère" [Lettre où rien n’a changé, un ingénieur africain]
*Walvis Bay, Namibie, 29 février 2060*

Cher vieux,
comme dit le proverbe « La plupart des gens préfèrent vivre avec de vieux problèmes, qu’avec de nouvelles solutions », mais ici on fait très fort : on utilise de nouvelles solutions pour entretenir de vieux problèmes. Comme ça personne ne se pose trop de questions. La géo-ingénierie* ça s’appelle. Maintenant, l’Homme contrôle le climat.

Grâce aux bonnes études que tu m’as incitées à faire, ça fait maintenant 10 ans que je travaille ici, à la nouvelle base de lancement internationale de Walvis Bay. Mon boulot consiste à envoyer des fusées pleines de dioxyde de soufre (SO2) dans la stratosphère : ça lui donne un petit coup de froid. Quand on peut, on envoie aussi des satellites miroirs dans l’espace. Une autre équipe s’occupe de provoquer la création de nuages marins [avec des tuyaux d'un kilomètre de long] qui réfléchissent la lumière. On se bat toujours pour nos créneaux de lancement de fusées. Faut bien qu’on y voit quelque chose pour les lancer, quand même !
Il y a encore une autre équipe qui disperse des micro-bulles réfléchissantes dans l’océan. Les écolos ont bien râlé au nom des derniers poissons, mais il y avait d’autres priorités. Il n’y a que deux bases astro-climatiques comme celle-ci dans le monde, la nôtre et celle de nos collègues de Longueuil, au Québec. Une dans l’hémisphère nord, une dans l’hémisphère sud. Elles sont toutes les deux placées sur des courants marins.
Tout ça, ça sert juste à renvoyer les rayons du soleil pour ne pas faire augmenter plus la température de notre petite planète. Ça coûte plusieurs montagnes, mais comme toutes les multinationales du monde nous achètent des crédits carbone*, et vu ce qu’elles émettent, on n'est pas près d’être à court de liquidités.
J’ai la sécurité de l’emploi, c’est sûr… Si je le demande, mes enfants, mes petits-enfants et mes arrières petits-enfants obtiendront même du travail ici. Car il ne faut surtout pas qu’on arrête, sinon on va se payer un coup de chaud puissance au cube*.
Mon cher vieux, je t’écris juste parce qu’aujourd’hui j’ai des doutes sur le chemin que tu m’as montré. Ce matin, le médecin m’a dit que j’avais un cancer. J’ai probablement trop manipulé de SO2. 10 ans, c’est passé vite. Peut-être que j’aurais dû être agriculteur, comme toi, ou même permaculteur*. J’aurais passé ma vie dans les champs. Si tu as ce message, pourras-tu le dire au jeune moi ?


## Intermède – une illustration
[Pub détournée :] Construisons un meilleur futur énergétique. Allons-y. / Let's build a better energy future. Let's go. 
[Photo d'une vue aérienne de champs d'exploitation de gaz de schistes]
[Photo : Bruce Gordon / EcoFlight / CC-BY-SA / https://www.flickr.com/photos/sfupamr/14601885300]


## Ép. 5 - "Vivants" [Lettre où on a changé de civilisation, une jeune adulte afghane (22 ans)]
*Koundouz, کندز, Afghanistan, 3 avril 2072* [dans une ville-forêt]

Salut l'ancien ! Je te pardonne. 
Je te pardonne la chaleur, je te pardonne la soif, je te pardonne la faim, je te pardonne le désert qui gagne et les torrents de pluies soudains. Car nous avons planté ensemble et vu grandir les graines de notre futur et de notre paix.

Tu es venu te battre chez moi et nous nous sommes battus contre toi, mais je te pardonne au nom de mes parents et grands-parents. Tu ne savais pas ! Tu te croyais séparé du monde, tu pensais que la matière était inerte, et donc tu traitais ton monde avec mépris comme une chose sans vie.
C'était avant, avant la 4e blessure* :
- La blessure cosmologique : Copernic, la terre n'est pas au centre de l'univers
- La blessure biologique : Darwin, l'Homme est le fruit de l'évolution et il est un animal comme les autres
- La blessure psychologique : Freud, l'Homme n'est pas maître de ses propres pensées, il est dominé par son inconscient
- La blessure neuro-quantique : Zhao, l'Homme n'est pas plus intelligent que ce qui l'entoure, toute matière est vivante et consciente, autant en nous qu'en dehors de nous, seule l'intensité de son interaction avec l'espace-temps nous donne l'impression de l'inerte ou du vivant à notre échelle (minéral, végétal, animal…).

Difficilement, nous avons appris que nous sommes fondamentalement la même vie que la terre sur laquelle nous nous tenons, que nous l’appelions Allah ou Gaïa, que nous sommes interdépendants, que ma conscience n'existe pas sans ta conscience et celle de l'arbre à côté de moi et celle de celui à l'autre bout de notre planète. Dès lors, tuer l'étranger ou payer pour polluer l'arbre lointain, c'est comme se détruire ou se blesser soi-même : ça n'a aucun sens et c'est suicidaire !
Et c'est à partir de là que nous avons commencé à pardonner, à guérir de la guerre et des horreurs du passé.

« L'éclatement de la bulle du transport maritime », suivi par « le grand arrêt » : c'est le joli nom que l'on a donné au choc de l'effondrement de l'économie mondialisée occidentale. J'avais 9 ans. En 2059, après des décennies de super-containers qui déplaçaient les marchandises du monde à un prix presque nul, la raréfaction des énergies fossiles n'a plus pu être compensée par les subventions et les trucages financiers, et le transport s'est brutalement mis à coûter cher, très cher. Toute votre civilisation s'est immobilisée en quelques semaines.
Mais pour nous, ça n'a pas changé grand-chose : nous étions déjà dans la misère, nous savions déjà ce qu'il fallait faire et nous ne dépendions que de nous-même. Alors, à notre façon, nous vous avons aidé à comprendre ce qu'une chrono-physicienne taïwanaise avait découvert, mais que nous savions depuis toujours, et nous avons planté tous ensemble les graines de notre futur. Et tout d'un coup, notre conscience de nous-même a changé. Tout d'un coup j'ai pu faire des études, moi une fille !

Je vois que tu ne comprends pas, je vais essayer de t'expliquer.
À ton époque, même si cela faisait presque un siècle qu'Einstein et ses collègues avaient commencé à la théoriser, on n'avait toujours pas compris la physique quantique. Vous pensiez que les photons et les électrons se comportaient aléatoirement, pouvaient être au niveau quantique à la fois présents et absents, ici et là… que le chat de Schrödinger était *à la fois* vivant et mort au même moment, ou qu'il l'était dans des mondes parralèles ! Tout cela parce que leur comportement étrange n'était pas déterminé par une autre force, une autre cause, et qu'ils échappaient à la logique de la physique du monde “normal”.
En absence de logique, votre seule explication était que les particules se comportent de manière aléatoire, mais avec une probabilité statistique qui permettait de prédire à peu près ce qui devait se produire. Même si être à la fois là et pas là au même moment, ça semble absurde, la statistique sauvait le scientifique !
Mais c'est parce que vous pensiez que les photons et les électrons étaient sans vie, inconscients, inertes, purement “mécaniques”. La solution est toute simple, et notre physique actuelle l’explique : c'est le photon qui choisit son électron. Même si cela te paraît inconcevable, c'est lui qui décide. Il est intelligent. Et nous partageons son intelligence en nous-même, dans les électrons qui parcourent nos neurones et s'échangent. Il y a des électrons dans tous les atomes, et il y a des atomes dans toute matière. Donc toute matière est intelligente et vivante, même si elle traverse l'espace-temps lentement à nos yeux, par exemple si elle est sous forme minérale.

Nous percevons cette intelligence et cette vie qui nous entoure en méditant. Ça nous donne la force de réparer ensemble nos blessures collectives, et celles de notre terre. Nous ne sommes plus séparés. Ne te laisse plus appeler consommateur, celui qui consomme et consume la vie autour de lui et en lui : fais-toi appeler jardinier, celui qui fait grandir la vie autour de lui et en lui. Alors tu trouveras que tu fais partie de la vie tout autour de toi, et tu ne te sentiras plus seul ni séparé de ta Terre, ni de tes proches, ni de tes lointains…
Au revoir, et la paix soit sur toi !

## Final – une illustration
une femme enceinte en train de faire l’amour - en dessous d'elle, l'ouvrier mexicain


[Notes]


